/**
 * Sequential version of hello world.
 * 
 * @author Kenyon Ralph <kenyon@kenyonralph.com>
 */
public class Prog1Seq {
    /**
     * Normal exit status.
     */
    private static final int EXIT_SUCCESS = 0;

    /**
     * Abnormal exit status.
     */
    private static final int EXIT_USAGE = 1;

    /**
     * Prints "Hello world" a given number of times. The number of times is given as the first command line argument.
     * @param args command line arguments.
     */
    public static void main(String[] args) {
	if (args.length != 1) {
	    printUsage();
	    System.exit(EXIT_USAGE);
	}

	int count = Integer.parseInt(args[0]);

	for (int i = 0; i < count; ++i) {
	    System.out.println("Hello world " + i);
	}

	System.exit(EXIT_SUCCESS);
    }

    /**
     * Prints usage information.
     */
    private static void printUsage() {
	System.out.println("Usage: " + Prog1Seq.class.getName() + " <count>");
	System.out.println("where <count> is an integer number of times to do the computation.");
    }
}
