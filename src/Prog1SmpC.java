import edu.rit.pj.IntegerForLoop;
import edu.rit.pj.ParallelRegion;
import edu.rit.pj.ParallelTeam;

/**
 * Parallel version C of hello world. The number of threads created is the same
 * as the number of times given to execute the computation.
 * 
 * @author Kenyon Ralph <kenyon@kenyonralph.com>
 */
public class Prog1SmpC {
    /**
     * Normal exit status.
     */
    private static final int EXIT_SUCCESS = 0;

    /**
     * Abnormal exit status.
     */
    private static final int EXIT_USAGE   = 1;

    /**
     * Number of computations to perform.
     */
    static int	       count;

    /**
     * Prints "Hello world" a given number of times. The number of times is
     * given as the first command line argument.
     * 
     * @param args command line arguments.
     * @throws Exception from Parallel Java somewhere.
     */
    public static void main(String[] args) throws Exception {
	if (args.length != 1) {
	    printUsage();
	    System.exit(EXIT_USAGE);
	}

	count = Integer.parseInt(args[0]);

	new ParallelTeam(count).execute(new ParallelRegion() {
	    @Override
	    public void run() throws Exception {
		this.execute(0, count - 1, new IntegerForLoop() {
		    @Override
		    public void run(int first, int last) {
			for (int i = first; i <= last; ++i) {
			    System.out.println("Hello world " + i);
			}
		    }
		});
	    }
	});

	System.exit(EXIT_SUCCESS);
    }

    /**
     * Prints usage information.
     */
    private static void printUsage() {
	System.out.println("Usage: " + Prog1SmpC.class.getName() + " <count>");
	System.out.println("where <count> is an integer number of times to do the computation.");
    }
}
